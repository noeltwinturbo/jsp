<%-- 
    Document   : PanelAdministrador
    Created on : 05-jul-2018, 11:31:54
    Author     : Noel
--%>

<%@page import="Utilidades.Estaticos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel administrador</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
        <style>
            body 
            {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translateX(-50%) translateY(-50%);
            }
        </style>
    </head>
    <body>

        <h1>Panel administrador</h1>
        
        <br/>

        <form>
            <button type="button" name="btBuscador" onclick="window.location = 'BuscadorAdministrador.jsp';">Buscador</button>
            <button type="button" name="btGestionarAlumnos" onclick="window.location = 'AlumnosAdministrador.jsp';">Gestionar alumnos</button>
            <button type="button" name="btGestionarCursos" onclick="window.location = 'CursosAdministrador.jsp';">Gestionar cursos</button>
            <button type="submit" name="btSalir" style="background-color: #f44336">Salir</button>

            <%
                if (request.getParameter("btSalir") != null) {
                    Estaticos.codigoAlumno = -1;
                    response.setContentType("text/html;charset=UTF-8");
                    Estaticos.message = "Sesión de alumno cerrada";
                    response.sendRedirect("Salida.jsp");
                }
            %>
        </form>

    </body>
</html>
