<%-- 
    Document   : ModificarAlumnoAdministrador
    Created on : 05-jul-2018, 11:38:11
    Author     : Noel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.Estaticos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar alumno</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body class="cuerpo">
        <h1>Modificar alumno</h1>

        <form action="ModificarAlumnoAdministrador" method="post">
            <%
                Class.forName(Estaticos.CLASS);
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                PreparedStatement stmt = conn.prepareStatement("select * from alumno where dni like ?");
                stmt.setString(1, request.getParameter("dni"));

                ResultSet rs = stmt.executeQuery();

                rs.next();
            %>
            <label><b>Código alumno</b></label>
            <input type="text" name="tbCodigoAlumno" value="<%= Integer.valueOf(rs.getString("codigoAlumno"))%>" required readonly>

            <label><b>Nombre</b></label>
            <input type="text" placeholder="Nombre" name="tbNombreAlumno" value="<%= rs.getString("nombre")%>" required>

            <label><b>Apellidos</b></label>
            <input type="text" placeholder="Apellidos" name="tbApellidosAlumno" value="<%= rs.getString("apellidos")%>" required>

            <label><b>DNI</b></label>
            <input type="text" placeholder="DNI" name="tbDNIAlumno" value="<%= rs.getString("dni")%>" required readonly>

            <label><b>Provincia</b></label>
            <input type="text" placeholder="Provincia" name="tbProvinciaAlumno" value="<%= rs.getString("provincia")%>" required>

            <label><b>Tipo de usuario</b></label>
            <select name="cbTipoUsuarioAlumno">
                <%
                    stmt = conn.prepareStatement("select tipoUsuario from Rol");

                    rs = stmt.executeQuery();
                    while (rs.next()) {
                        String name = rs.getString("tipoUsuario");
                %>
                <option value="<%=name%>"><%=name%></option>
                <%
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                %>
            </select></br>

            <button type="submit">Aceptar</button>
            <button type="button" name="btCancelar" onclick="window.location = 'AlumnosAdministrador.jsp';" class="cancelbtn3">Cancelar</button>
        </form>
    </body>
</html>
