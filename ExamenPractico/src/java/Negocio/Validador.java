/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Utilidades.Estaticos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Noel
 */
public class Validador {

    public static boolean checkLogin(int codigoAlumno, boolean source) {
        boolean check = false;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            String query;

            if (source) {
                query = "select * from alumno where codigoAlumno = ? and eliminado = 0 and tipoUsuario like 'admin'";
            } else {
                query = "select * from alumno where codigoAlumno = ? and eliminado = 0 and codigoAlumno != 1";
            }

            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, codigoAlumno);

            ResultSet rs = stmt.executeQuery();
            check = rs.next();
            
            if (check){
                Estaticos.codigoAlumno = codigoAlumno;
                if (source){
                    Estaticos.rol = true;
                }
                else{
                    Estaticos.rol = false;
                }
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }

        return check;
    }

    public static boolean checkExistsAlumno(String dni) {
        boolean check = false;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("select * from alumno where dni like ?");
            stmt.setString(1, dni);

            ResultSet rs = stmt.executeQuery();
            check = rs.next();

            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
        }

        return check;
    }
    
    public static boolean checkExistsCurso(int codigoCurso) {
        boolean check = false;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("select * from curso where codigoCurso = ?");
            stmt.setInt(1, codigoCurso);

            ResultSet rs = stmt.executeQuery();
            check = rs.next();

            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
        }

        return check;
    }

    public static boolean checkExistsInscripcion(int codigoCurso) {
        boolean check = false;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("select * from CursoAlumno where codigoAlumno = ? and codigoCurso = ?");
            stmt.setInt(1, Estaticos.codigoAlumno);
            stmt.setInt(2, codigoCurso);

            ResultSet rs = stmt.executeQuery();
            check = rs.next();

            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }

        return check;
    }

}
