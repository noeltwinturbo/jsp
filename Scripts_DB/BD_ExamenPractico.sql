create database Escuela;

use Escuela;

create table Curso
	(
		codigoCurso int not null,
        nombre varchar(250) not null,
        eliminado bool not null,
        PRIMARY KEY(codigoCurso)
    );
    
create table Rol
	(
        tipoUsuario varchar(100) not null,
        PRIMARY KEY(tipoUsuario)
    );

create table Alumno
	(
		codigoAlumno int AUTO_INCREMENT not null,
		dni varchar(15) not null,
        nombre varchar(250) not null,
        apellidos varchar(250) not null,
        provincia varchar(250) null,
        tipoUsuario varchar(100) not null,
        eliminado bool not null,
        PRIMARY KEY (codigoAlumno),
        FOREIGN KEY (tipoUsuario) references Rol(tipoUsuario),
        UNIQUE (dni)
    );
    
create table CursoAlumno
	(
		codigoCurso int not null,
        codigoAlumno int not null,
        PRIMARY KEY(codigoCurso, codigoAlumno),
        FOREIGN KEY(codigoCurso) references Curso(codigoCurso) ON DELETE CASCADE,
        FOREIGN KEY(codigoAlumno) references Alumno(codigoAlumno) ON DELETE CASCADE
    );
    
insert Rol (tipoUsuario) values ('admin'), ('user');

insert Alumno (dni, nombre, apellidos, tipoUsuario, eliminado) values ('44653369L', 'admin', 'admin', 'admin', false);