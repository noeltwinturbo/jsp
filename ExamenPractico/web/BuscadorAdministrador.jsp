<%-- 
    Document   : BuscadorAdministrador
    Created on : 05-jul-2018, 11:39:20
    Author     : Noel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Utilidades.Estaticos"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <BODY style="margin: 5% 10%">

        <div style="float: left; width: 45%">
            <H1>Búsqueda cursos alumno</H1>

            <form>
                <select name="cbAlumnos">
                    <%
                        Class.forName(Estaticos.CLASS);
                        Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                        PreparedStatement stmt = conn.prepareStatement("select dni from alumno where codigoAlumno != 0 and eliminado = 0");

                        ResultSet rs = stmt.executeQuery();
                        while (rs.next()) {
                            String name = rs.getString("dni");
                    %>
                    <option value="<%=name%>"><%=name%></option>
                    <%
                        }
                    %>
                </select></br>

                <button type="submit" name="btBuscar">Buscar</button>       
            </form>

            <H1>Cursos</H1>
                <%
                    stmt = conn.prepareStatement("select curso.codigoCurso, curso.nombre, curso.eliminado from curso join CursoAlumno on curso.codigoCurso = cursoAlumno.codigoCurso join alumno on cursoAlumno.codigoAlumno = alumno.codigoAlumno where curso.eliminado = 0 and dni = ?");
                    stmt.setString(1, request.getParameter("cbAlumnos"));
                    rs = stmt.executeQuery();
                %>

            <TABLE>
                <TR>
                    <TH>ID</TH>
                    <TH>Nombre</TH>
                    <TH>Estado</TH>
                </TR>
                <%
                    while (rs.next()) {
                %>
                <TR>
                    <TD> <%= rs.getInt("codigoCurso")%></td>
                    <TD> <%= rs.getString("nombre")%></TD>
                        <%
                            String aux;
                            if (rs.getBoolean("eliminado")) {
                                aux = "eliminado";
                            } else {
                                aux = "vigente";
                            }
                        %>
                    <TD> <%= aux%></TD>
                </TR>
                <%
                    }
                %>
            </TABLE></br></br>

        </div>
        <div style="float: right; width: 50%">

            <H1>Búsqueda curso alumnos</H1>

            <form>
                <select name="cbCursos">
                    <%
                        stmt = conn.prepareStatement("select codigoCurso, nombre from curso where eliminado = 0");

                        rs = stmt.executeQuery();
                        while (rs.next()) {
                            String name = rs.getInt("codigoCurso") + " - " + rs.getString("nombre");
                    %>
                    <option value="<%=name%>"><%=name%></option>
                    <%
                        }
                    %>
                </select></br>

                <button type="submit" name="btBuscar">Buscar</button>       
            </form>

            <H1>Alumnos</H1>
                <%
                    stmt = conn.prepareStatement("select alumno.codigoAlumno, alumno.Nombre, alumno.Apellidos, alumno.dni, alumno.Provincia, alumno.tipoUsuario, alumno.eliminado from curso join CursoAlumno on curso.codigoCurso = cursoAlumno.codigoCurso join alumno on cursoAlumno.codigoAlumno = alumno.codigoAlumno where curso.eliminado = 0 and curso.codigoCurso = ?");
                    int index = -1;
                    try {
                        index = Integer.valueOf(request.getParameter("cbCursos").split(" - ")[0]);
                    } catch (Exception ex) {
                    }
                    stmt.setInt(1, index);
                    rs = stmt.executeQuery();
                %>

            <TABLE>
                <TR>
                    <TH>ID</TH>
                    <TH>Nombre</TH>
                    <TH>Apellidos</TH>
                    <TH>DNI</TH>
                    <TH>Provincia</TH>
                    <TH>Tipo de usuario</TH>
                    <TH>Estado</TH>
                </TR>
                <%
                    while (rs.next()) {
                %>
                <TR>
                    <TD> <%= rs.getInt("codigoAlumno")%></td>
                    <TD> <%= rs.getString("nombre")%></TD>
                    <TD> <%= rs.getString("apellidos")%></TD>
                    <TD> <%= rs.getString("dni")%></TD>
                    <TD> <%= rs.getString("provincia")%></TD>
                    <TD> <%= rs.getString("tipoUsuario")%></TD>
                        <%
                            String aux;
                            if (rs.getBoolean("eliminado")) {
                                aux = "eliminado";
                            } else {
                                aux = "vigente";
                            }
                        %>
                    <TD> <%= aux%></TD>

                </TR>
                <%
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                %>
            </TABLE></br></br>
        </div>

        <button type="button" name="btAtras" style="background-color: #FF8C00" onclick="window.location = 'PanelAdministrador.jsp';">Atrás</button> 
    </BODY>
</html>
