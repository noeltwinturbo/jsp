/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Negocio.Modificar;
import Negocio.Validador;
import Utilidades.Estaticos;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Noel
 */
@WebServlet(name = "ModificarAlumnoAdministrador", urlPatterns = {"/ModificarAlumnoAdministrador"})
public class ModificarAlumnoAdministrador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        int codigoAlumno = Integer.valueOf(request.getParameter("tbCodigoAlumno"));
        String nombre = request.getParameter("tbNombreAlumno");
        String apellidos = request.getParameter("tbApellidosAlumno");
        String provincia = request.getParameter("tbProvinciaAlumno");
        String tipoUsuario = request.getParameter("cbTipoUsuarioAlumno");

        if (Modificar.modificarAlumno(nombre, apellidos, provincia, tipoUsuario, codigoAlumno, true) == 1) {
            Estaticos.message = "Alumno modificado correctamente";
        } else {
            Estaticos.message = "Ha habido un error a la hora de modificar el alumno";
        }

        response.sendRedirect("Salida.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
