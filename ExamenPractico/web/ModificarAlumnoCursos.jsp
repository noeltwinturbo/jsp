<%-- 
    Document   : ModificarAlumnoCursos
    Created on : 05-jul-2018, 13:06:40
    Author     : Noel
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.Estaticos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dar de baja curso</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body class="cuerpo">
        <h1>Dar de baja curso</h1>

        <form method="post" action="DarBaja">
            <label><b>DNI alumno</b></label>
            <input type="text" name="tbDNIAlumno" value="<%= request.getParameter("dni")%>" required readonly>

            <label><b>Selecciona el curso</b></label>
            <select name="cbCursos">
                <%
                    Class.forName(Estaticos.CLASS);
                    Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                    PreparedStatement stmt = conn.prepareStatement("select curso.codigoCurso, curso.nombre from curso join CursoAlumno on curso.codigoCurso = cursoAlumno.codigoCurso join alumno on cursoAlumno.codigoAlumno = alumno.codigoAlumno where curso.eliminado = 0 and dni = ?");
                    stmt.setString(1, request.getParameter("dni"));

                    ResultSet rs = stmt.executeQuery();
                    while (rs.next()) {
                        String name = rs.getInt("codigoCurso") + " - " + rs.getString("nombre");
                %>
                <option value="<%=name%>"><%=name%></option>
                <%
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                %>
            </select></br>
            <button type="submit" name="btDarBaja">Dar de baja</button>
            <button type="button" name="btAtras" style="background-color: #FF8C00" onclick="window.location = 'AlumnosAdministrador.jsp';">Atrás</button> 
        </form>

    </body>
</html>
