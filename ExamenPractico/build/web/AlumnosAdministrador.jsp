<%-- 
    Document   : AlumnosAdministrador
    Created on : 05-jul-2018, 11:37:33
    Author     : Noel
--%>

<%@page import="Negocio.Modificar"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.Estaticos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumnos</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <BODY class="cuerpo">

        <H1>Alumnos</H1>
            <%
                Class.forName(Estaticos.CLASS);
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                PreparedStatement stmt = conn.prepareStatement("select * from alumno");

                ResultSet rs = stmt.executeQuery();
            %>

        <TABLE>
            <TR>
                <TH>ID</TH>
                <TH>Nombre</TH>
                <TH>Apellidos</TH>
                <TH>DNI</TH>
                <TH>Provincia</TH>
                <TH>Tipo de usuario</TH>
                <TH>Estado</TH>
                <TH>Cursos</TH>
            </TR>
            <%
                while (rs.next()) {
                    int codigoAlumno = rs.getInt("codigoAlumno");
            %>
            <TR>
                <TD> <%= codigoAlumno%></td>
                <TD> <%= rs.getString("nombre")%></TD>
                <TD> <%= rs.getString("apellidos")%></TD>
                <TD> <%= rs.getString("dni")%></TD>
                <TD> <%= rs.getString("provincia")%></TD>
                <TD> <%= rs.getString("tipoUsuario")%></TD>
                    <%
                        String aux;
                        if (rs.getBoolean("eliminado")) {
                            aux = "eliminado";
                        } else {
                            aux = "vigente";
                        }
                    %>
                <TD> <%= aux%></TD>

                <%
                    StringBuilder lista = new StringBuilder();
                    Connection conn2 = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                    PreparedStatement stmt2 = conn2.prepareStatement("select nombre from Curso join CursoAlumno on Curso.codigoCurso = CursoAlumno.codigoCurso where CursoAlumno.codigoAlumno like ?");
                    stmt2.setInt(1, codigoAlumno);

                    ResultSet rs2 = stmt2.executeQuery();
                    while (rs2.next()) {
                        lista.append(rs2.getString("nombre")).append(", ");
                    }
                    if (lista.length() > 0) {
                        lista.replace(lista.length() - 2, lista.length(), "");
                    }
                    rs2.close();
                    stmt2.close();
                    conn2.close();
                %>
                <TD><%= lista.toString()%></TD>

            </TR>
            <%
                }
            %>
        </TABLE></br></br>

        <form method="POST">

            <label><b>Alumno</b></label>
            <select name="cbAlumnos">
                <%
                    stmt = conn.prepareStatement("select dni from alumno where eliminado = 0 and codigoAlumno != 1");

                    rs = stmt.executeQuery();
                    while (rs.next()) {
                        String name = rs.getString("dni");
                %>
                <option value="<%=name%>"><%=name%></option>
                <%
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                %>
            </select></br>

            <button type="submit" name="btModificarAlumnoCursos">Gestionar cursos alumno</button>
            <button type="submit" name="btModificarAlumno">Modificar alumno</button>
            <button type="submit" name="btEliminarAlumno" class="cancelbtn3">Eliminar alumno</button> 
            <button type="button" name="btAtras" style="background-color: #FF8C00" onclick="window.location = 'PanelAdministrador.jsp';">Atrás</button> 

            <%
                if (request.getParameter("cbAlumnos") != null) {
                    if (request.getParameter("btModificarAlumno") != null) {
                        response.sendRedirect("ModificarAlumnoAdministrador.jsp?dni=" + request.getParameter("cbAlumnos"));
                    } else if (request.getParameter("btModificarAlumnoCursos") != null) {
                        response.sendRedirect("ModificarAlumnoCursos.jsp?dni=" + request.getParameter("cbAlumnos"));
                    } else if (request.getParameter("btEliminarAlumno") != null) {
                        if (Modificar.removeAlumno(request.getParameter("cbAlumnos")) == 1) {
                            Estaticos.message = "Alumno eliminado con éxito";
                        } else {
                            Estaticos.message = "Ha habido un error a la hora de eliminar el alumno";
                        }
                        response.sendRedirect("Salida.jsp");
                    }
                } else if (request.getParameter("btModificarAlumno") != null
                        || request.getParameter("btModficiarAlumnoCursos") != null
                        || request.getParameter("btEliminarAlumno") != null) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('No hay ningún alumno que gestionar');");
                    out.println("location='AlumnosAdministrador.jsp';");
                    out.println("</script>");
                }

            %>
        </form>

    </BODY>
</html>
