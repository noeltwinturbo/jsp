<%-- 
    Document   : Inicio
    Created on : 05-jul-2018, 9:44:41
    Author     : Noel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicio</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body>

        <div class="base">
            <img src="http://i0.wp.com/blog.adeel.io/wp-content/uploads/2016/11/java.png?fit=357%2C225"/>

            </br>
            </br>
            </br>
            </br>
            </br>

            <button type="button" name="btComenzar" onclick="window.location = 'DatosPersonales.jsp';">Cursos</button>
            <button onclick="document.getElementById('id01').style.display = 'block'">Administrar</button>

        </div>

        <div id="id01" class="modal">

            <form class="modal-content animate" action="LoginAdmin" method="post">
                <div class="imgcontainer">
                    <span onclick="document.getElementById('id01').style.display = 'none'" class="close">&times;</span>
                    <img src="https://www.w3schools.com/howto/img_avatar2.png" class="avatar">
                </div>

                <div class="container">
                    <label><b>Introduce tu código de administrador</b></label>
                    <input type="text" placeholder="Código" name="tbCodigoAlumno" required>

                    <button type="submit">Aceptar</button>
                    <button type="reset">Borrar</button>
                </div>

                <div class="container" style="background-color:#f1f1f1">
                    <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">Cancelar</button>
                </div>
            </form>

        </div>

        <script>
            // Get the modal
            var modal = document.getElementById('id01');

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target === modal) {
                    modal.style.display = "none";
                }
            }
        </script>

    </body>
</html>
