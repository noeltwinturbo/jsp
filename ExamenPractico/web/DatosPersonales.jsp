<%-- 
    Document   : DatosPersonales
    Created on : 05-jul-2018, 9:45:11
    Author     : Noel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Acceso</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body>

        <div class="base">

            <h1>Bienvenid@</h1>
            
            <br/>
            
            <button onclick="document.getElementById('id01').style.display = 'block'">Entrar</button>
            <button onclick="document.getElementById('id02').style.display = 'block'">Registrarse</button>
            <button name="btAtras" onclick="window.location = 'Inicio.jsp';" class="cancelbtn3">Atrás</button>

        </div>

        <!-- LOGIN -->

        <div id="id01" class="modal">

            <form class="modal-content animate" action="Login" method="post">
                <div class="imgcontainer">
                    <span onclick="document.getElementById('id01').style.display = 'none'" class="close">&times;</span>
                    <img src="https://www.w3schools.com/howto/img_avatar2.png" class="avatar">
                </div>

                <div class="container">
                    <label><b>Introduce tu código de alumno</b></label>
                    <input type="text" placeholder="Código" name="tbCodigoAlumno" required>

                    <button type="submit">Aceptar</button>
                    <button type="reset">Borrar</button>
                </div>

                <div class="container" style="background-color:#f1f1f1">
                    <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">Cancelar</button>
                </div>
            </form>

        </div>

        <script>
            // Get the modal
            var modal = document.getElementById('id01');

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target === modal) {
                    modal.style.display = "none";
                }
            }
        </script>

        <!-- REGISTRO -->

        <div id="id02" class="modal">

            <span onclick="document.getElementById('id02').style.display = 'none'" class="close2" title="Close Modal">&times;</span>
            <form class="modal-content animate" action="RegistroAlumno" method="post">
                <div class="container">
                    <h1>Registro</h1>
                    <hr>

                    <label><b>DNI</b></label>
                    <input type="text" placeholder="DNI" name="tbDNIRegistro" required>

                    <label><b>Nombre</b></label>
                    <input type="text" placeholder="Nombre" name="tbNombreRegistro" required>

                    <label><b>Apellidos</b></label>
                    <input type="text" placeholder="Apellidos" name="tbApellidosRegistro" required>

                    <label><b>Provincia</b></label>
                    <input type="text" placeholder="Provincia" name="tbProvinciaRegistro" required>

                    <div class="clearfix">
                        <button type="submit" class="signupbtn">Aceptar</button>
                        <button type="reset" class="signupbtn">Borrar</button>
                        <button type="button" onclick="document.getElementById('id02').style.display = 'none'" class="cancelbtn2">Cancelar</button>
                    </div>
                </div>
            </form>

        </div>
    </body>
</html>
