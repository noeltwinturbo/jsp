<%-- 
    Document   : Cursos
    Created on : 05-jul-2018, 9:45:51
    Author     : Noel
--%>


<%@page import="Utilidades.Estaticos"%>
<%@page import="Negocio.Registrar"%>
<%@page import="Negocio.Validador"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cursos</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body class="cuerpo">

        <h1>Mis cursos</h1>

        <%
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("select codigoCurso, nombre from curso where codigoCurso in (select codigoCurso from CursoAlumno where codigoAlumno = ?)");
            stmt.setInt(1, Estaticos.codigoAlumno);

            ResultSet rs = stmt.executeQuery();
        %>

        <TABLE>
            <TR>
                <TH>ID</TH>
                <TH>Nombre</TH>
            </TR>
            <%
                while (rs.next()) {
                    int codigoCurso = rs.getInt("codigoCurso");
            %>
            <TR>
                <TD> <%= codigoCurso%></td>
                <TD> <%= rs.getString("nombre")%></TD>
            </TR>
            <%
                }
            %>
        </TABLE></br></br>

        <label><b>Seleccione un curso:</b></label>

        <form>
            <select name="cbCursos">
                <%
                    Class.forName(Estaticos.CLASS);
                    conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                    stmt = conn.prepareStatement("select codigoCurso, nombre from curso where eliminado = 0");

                    rs = stmt.executeQuery();
                    while (rs.next()) {
                        String name = rs.getInt("codigoCurso") + " - " + rs.getString("nombre");
                %>
                <option value="<%=name%>"><%=name%></option>
                <%
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                %>
            </select></br>
            <button type="submit" name="btAceptar">Aceptar</button>     
            <button type="button" name="btModificarAlumno" onclick="window.location = 'ModificarAlumno.jsp';">Modificar alumno</button>
            <button type="submit" name="btSalir" class="cancelbtn3">Salir</button> 

            <%
                if (request.getParameter("btAceptar") != null) {
                    response.setContentType("text/html;charset=UTF-8");

                    if (request.getParameter("cbCursos") != null) {
                        String aux = request.getParameter("cbCursos");
                        int curso = Integer.valueOf(aux.split(" - ")[0]);

                        if (!Validador.checkExistsInscripcion(curso)) {
                            if (Registrar.inscripcionCurso(curso) == 1) {
                                Estaticos.message = "El alumno se ha inscrito en el curso " + aux.split(" - ")[1];
                            } else {
                                Estaticos.message = "Ha habido un error a la hora de inscribirse en el curso";
                            }
                        } else {
                            Estaticos.message = "El alumno ya está inscrito en el curso " + aux.split(" - ")[1];
                        }
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('No hay ningún curso en el que inscribirse');");
                        out.println("location='Cursos.jsp';");
                        out.println("</script>");
                    }

                    response.sendRedirect("Salida.jsp");
                } else if (request.getParameter("btSalir") != null) {
                    Estaticos.codigoAlumno = -1;
                    response.setContentType("text/html;charset=UTF-8");
                    Estaticos.message = "Sesión de alumno cerrada";
                    response.sendRedirect("Salida.jsp");
                }
            %>
        </form>
    </body>
</html>
