<%-- 
    Document   : Salida
    Created on : 05-jul-2018, 9:46:22
    Author     : Noel
--%>

<%@page import="Utilidades.Estaticos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aviso</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
        <style>
            body 
            {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translateX(-50%) translateY(-50%);
            }
        </style>
    </head>
    <BODY>

        <form method="POST">
            <h1><%=Estaticos.message%></h1>

            <br/>

            <button type="submit" name="btVolver">Volver</button>

            <%
                if (request.getParameter("btVolver") != null) {
                    response.setContentType("text/html;charset=UTF-8");
                    if (Estaticos.codigoAlumno != -1) {
                        if (Estaticos.rol) {
                            response.sendRedirect("PanelAdministrador.jsp");
                        } else {
                            response.sendRedirect("Cursos.jsp");
                        }
                    } else {
                        response.sendRedirect("Inicio.jsp");
                    }
                }
            %> 
        </form>    

    </BODY>
</html>
