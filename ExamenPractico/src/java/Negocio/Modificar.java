/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Utilidades.Estaticos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Noel
 */
public class Modificar {
    
    public static int removeCurso(int codigoCurso) {
        int r = 0;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("update curso set eliminado = 1 where codigoCurso like ?");
            stmt.setInt(1, codigoCurso);

            r = stmt.executeUpdate();
            
            if (r == 1){
                stmt = conn.prepareStatement("delete from cursoAlumno where codigoCurso = ?");
                stmt.setInt(1, codigoCurso);

                stmt.executeUpdate();
            }

            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }
        
        return r;
    }
    
    public static int removeAlumno(String dni) {
        int r = 0;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("update alumno set eliminado = 1 where dni like ?");
            stmt.setString(1, dni);

            r = stmt.executeUpdate();
            
            if (r == 1){
                stmt = conn.prepareStatement("delete from cursoAlumno where codigoAlumno = (select codigoAlumno from alumno where dni like ?)");
                stmt.setString(1, dni);

                stmt.executeUpdate();
            }

            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }
        
        return r;
    }
    
    public static int modificarCurso(int codigoCurso, String nombre) {
        int r = 0;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("update curso set nombre = ? where codigoCurso like ?");
            stmt.setString(1, nombre);
            stmt.setInt(2, codigoCurso);

            r = stmt.executeUpdate();

            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }
        
        return r;
    }
    
    public static int modificarAlumno(String nombre, String apellidos, String provincia, String tipoUsuario, int codigoAlumno, boolean source) {
        int r = 0;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            String query;

            if (source) {
                query = "update alumno set nombre = ?, apellidos = ?, provincia = ?, tipoUsuario = ? where codigoAlumno like ?";
            } else {
                query = "update alumno set nombre = ?, apellidos = ?, provincia = ? where codigoAlumno like ?";
            }
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, nombre);
            stmt.setString(2, apellidos);
            stmt.setString(3, provincia);
            if (source) {
                stmt.setString(4, tipoUsuario);
                stmt.setInt(5, codigoAlumno);
            } else {
                stmt.setInt(4, codigoAlumno);
            }

            r = stmt.executeUpdate();

            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }
        
        return r;
    }
    
    public static int darBaja(int codigoCurso, String dni) {
        int r = 0;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("delete from cursoAlumno where codigoCurso = ? and codigoAlumno = (select codigoAlumno from alumno where dni = ?)");
            stmt.setInt(1, codigoCurso);
            stmt.setString(2, dni);
           
            r = stmt.executeUpdate();

            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }
        
        return r;
    }
    
}
