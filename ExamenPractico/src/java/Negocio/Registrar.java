/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Utilidades.Estaticos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Noel
 */
public class Registrar {

    public static int insertAlumno(String dni, String nombre, String apellidos, String pronvincia) {
        int r = 0;
        try {
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("insert alumno (dni, nombre, apellidos, provincia, tipoUsuario, eliminado) values (?,?,?,?,?,?)");
            stmt.setString(1, dni);
            stmt.setString(2, nombre);
            stmt.setString(3, apellidos);
            stmt.setString(4, pronvincia);
            stmt.setString(5, "user");
            stmt.setInt(6, 0);

            r = stmt.executeUpdate();

            if (r == 1) {
                stmt = conn.prepareStatement("select codigoAlumno from alumno where dni like ?");
                stmt.setString(1, dni);

                ResultSet rs = stmt.executeQuery();

                if (rs.next()) {
                    Estaticos.codigoAlumno = rs.getInt("codigoAlumno");
                }
                rs.close();
            }

            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) { }

        return r;
    }
    
    public static int insertCurso(int codigoCurso, String nombre){
        int r = 0;
         try{
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("insert curso (codigoCurso, nombre, eliminado) values (?,?,?)");
            stmt.setInt(1, codigoCurso);
            stmt.setString(2, nombre);
            stmt.setInt(3, 0);
            
            r = stmt.executeUpdate();  
            
            stmt.close();
            conn.close();
        }
        catch(ClassNotFoundException | SQLException ex) { }
         
        return r;
    }
    
    public static int inscripcionCurso(int codigoCurso){
        int r = 0;
         try{
            Class.forName(Estaticos.CLASS);
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

            PreparedStatement stmt = conn.prepareStatement("insert CursoAlumno (codigoCurso, codigoAlumno) values (?,?)");
            stmt.setInt(1, codigoCurso);
            stmt.setInt(2, Estaticos.codigoAlumno);
            
            r = stmt.executeUpdate();  
            
            stmt.close();
            conn.close();
        }
        catch(ClassNotFoundException | SQLException ex) { }
         
        return r;
    }

}
