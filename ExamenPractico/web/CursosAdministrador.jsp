<%-- 
    Document   : CursosAdministrador
    Created on : 05-jul-2018, 11:36:36
    Author     : Noel
--%>

<%@page import="Negocio.Modificar"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.Estaticos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cursos</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <BODY class="cuerpo">

        <H1>Cursos</H1>
            <%
                Class.forName(Estaticos.CLASS);
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                PreparedStatement stmt = conn.prepareStatement("select * from curso");

                ResultSet rs = stmt.executeQuery();
            %>

        <TABLE>
            <TR>
                <TH>ID</TH>
                <TH>Nombre</TH>
                <TH>Estado</TH>
                <TH>Alumnos</TH>
            </TR>
            <%
                while (rs.next()) {
                    int codigoCurso = rs.getInt("codigoCurso");
            %>
            <TR>
                <TD> <%= codigoCurso%></td>
                <TD> <%= rs.getString("nombre")%></TD>
                    <%
                        String aux;
                        if (rs.getBoolean("eliminado")) {
                            aux = "eliminado";
                        } else {
                            aux = "vigente";
                        }
                    %>
                <TD> <%= aux%></TD>

                <%
                    StringBuilder lista = new StringBuilder();
                    Connection conn2 = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                    PreparedStatement stmt2 = conn2.prepareStatement("select nombre, apellidos from CursoAlumno join alumno on alumno.codigoAlumno = CursoAlumno.codigoAlumno where codigoCurso like ?");
                    stmt2.setInt(1, codigoCurso);

                    ResultSet rs2 = stmt2.executeQuery();
                    while (rs2.next()) {
                        lista.append(rs2.getString("nombre") + " " + rs2.getString("apellidos")).append(", ");
                    }
                    if (lista.length() > 0) {
                        lista.replace(lista.length() - 2, lista.length(), "");
                    }
                    rs2.close();
                    stmt2.close();
                    conn2.close();
                %>
                <TD><%= lista.toString()%></TD>

            </TR>
            <%
                }
            %>
        </TABLE></br></br>

        <form method="POST">

            <label><b>Curso</b></label>
            <select name="cbCursos">
                <%
                    stmt = conn.prepareStatement("select codigoCurso, nombre from curso where eliminado = 0");

                    rs = stmt.executeQuery();
                    while (rs.next()) {
                        String name = rs.getInt("codigoCurso") + " - " + rs.getString("nombre");
                %>
                <option value="<%=name%>"><%=name%></option>
                <%
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                %>
            </select></br>

            <button type="button" name="btNuevoCurso" onclick="window.location = 'RegistrarCurso.jsp';">Nuevo curso</button> 
            <button type="submit" name="btModificarCurso">Modificar curso</button>            
            <button type="submit" name="btEliminarCurso" class="cancelbtn3">Eliminar curso</button> 
            <button type="button" name="btAtras" style="background-color: #FF8C00" onclick="window.location = 'PanelAdministrador.jsp';">Atrás</button> 

            <%
                if (request.getParameter("cbCursos") != null) {
                    String aux = request.getParameter("cbCursos");
                    int curso = Integer.valueOf(aux.split(" - ")[0]);

                    if (request.getParameter("btModificarCurso") != null) {
                        response.sendRedirect("ModificarCurso.jsp?codigoCurso=" + curso);
                    } else if (request.getParameter("btEliminarCurso") != null) {
                        if (Modificar.removeCurso(curso) == 1) {
                            Estaticos.message = "Curso eliminado con éxito";
                        } else {
                            Estaticos.message = "Ha habido un error a la hora de eliminar el curso";
                        }
                        response.sendRedirect("Salida.jsp");
                    }
                } else if (request.getParameter("btModificarCurso") != null
                        || request.getParameter("btEliminarCurso") != null) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('No hay ningún curso que gestionar');");
                    out.println("location='CursosAdministrador.jsp';");
                    out.println("</script>");
                }

            %>
        </form>

    </BODY>
</html>
