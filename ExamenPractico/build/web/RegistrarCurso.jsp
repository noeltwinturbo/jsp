<%-- 
    Document   : RegistrarCurso
    Created on : 05-jul-2018, 11:38:34
    Author     : Noel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar curso</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body class="cuerpo">
        <h1>Registrar curso</h1>

        <form action="RegistroCurso" method="post">
            <label><b>Código curso</b></label>
            <input type="text" placeholder="Código" name="tbCodigoCurso" required>
            
            <label><b>Nuevo nombre</b></label>
            <input type="text" placeholder="Nombre" name="tbNombreCurso" required>

            <button type="submit">Aceptar</button>
            <button type="reset">Borrar</button>
            <button type="button" name="btCancelar" onclick="window.location = 'CursosAdministrador.jsp';" class="cancelbtn3">Cancelar</button>
        </form>
    </body>
</html>
