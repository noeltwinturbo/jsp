<%-- 
    Document   : ModificarCurso
    Created on : 05-jul-2018, 11:38:48
    Author     : Noel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="Utilidades.Estaticos"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar curso</title>
        <link rel="stylesheet" type="text/css" href="CSS\Styles.css">
    </head>
    <body class="cuerpo">
        <h1>Modificar curso</h1>

        <form action="ModificarCurso" method="post">
            <%
                Class.forName(Estaticos.CLASS);
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + Estaticos.URL + "/" + Estaticos.ESCUELA + "?useSSL=false&serverTimezone=UTC", Estaticos.USER, Estaticos.PASSWORD);

                PreparedStatement stmt = conn.prepareStatement("select nombre from curso where codigoCurso like ?");
                stmt.setInt(1, Integer.valueOf(request.getParameter("codigoCurso")));

                ResultSet rs = stmt.executeQuery();

                rs.next();
            %>
            <label><b>Código curso</b></label>
            <input type="text" name="tbCodigoCurso" value="<%= Integer.valueOf(request.getParameter("codigoCurso"))%>" required readonly>
            
            <label><b>Nuevo nombre</b></label>
            <input type="text" placeholder="Nombre" name="tbNombreCurso" value="<%= rs.getString("nombre") %>" required>

            <button type="submit">Aceptar</button>
            <button type="button" name="btCancelar" onclick="window.location = 'CursosAdministrador.jsp';" class="cancelbtn3">Cancelar</button>
        </form>
    </body>
</html>
