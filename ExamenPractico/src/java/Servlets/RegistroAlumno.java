/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Negocio.Registrar;
import Negocio.Validador;
import Utilidades.Estaticos;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Noel
 */
@WebServlet(name = "RegistroAlumno", urlPatterns = {"/RegistroAlumno"})
public class RegistroAlumno extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String dni = request.getParameter("tbDNIRegistro");
        String nombre = request.getParameter("tbNombreRegistro");
        String apellidos = request.getParameter("tbApellidosRegistro");
        String provincia = request.getParameter("tbProvinciaRegistro");        
        
        if(!Validador.checkExistsAlumno(dni))
        {
            if (Registrar.insertAlumno(dni, nombre, apellidos, provincia) == 1){
                Estaticos.message = "Alumno dado de alta correctamente. Código de aceso: " + Estaticos.codigoAlumno;
            }
            else{
                Estaticos.message = "Ha habido un error a la hora de añadir el alumno";
            }
        }
        else
        {
           Estaticos.message = "Ya existe un alumno con este DNI";
        }
        response.sendRedirect("Salida.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
